from django.test import TestCase, Client
from .models import provinsi
from django.urls import resolve
from .views import showprovinsiView

# Create your tests here.
class TestingStats(TestCase):
    def test_url_stats(self):
        response = Client().get('/stats/')
        self.assertEquals(response.status_code, 200)

    def test_model_stats(self):
        provinsi.objects.create(nama="test", mati="0", positif="0", sembuh="0")
        jumlahprovinsi = provinsi.objects.all().count()
        self.assertEquals(jumlahprovinsi, 1)

    def test_views_stat(self):
        found = resolve('/stats/')
        self.assertEquals(found.func.view_class , showprovinsiView)

    def test_template_html_stats(self):
        response = Client().get('/stats/')
        self.assertTemplateUsed(response, 'stats/face.html')