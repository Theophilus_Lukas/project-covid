from django import forms
from .models import Checkin

class CheckinForm(forms.ModelForm):
    class Meta:
        model = Checkin
        fields=['Subject','Date','Message']
        widgets={
            'Subject' : forms.TextInput(attrs={'class':'form-control'}),
            'Date' :forms.TextInput(attrs={'class':'form-control'}),
            'Message' :forms.TextInput(attrs={'class':'form-control'}),
        }