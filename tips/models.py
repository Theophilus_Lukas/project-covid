from django.db import models

# Create your models here.
class Checkin(models.Model):
    Subject = models.CharField(max_length=50)
    Date = models.CharField(max_length=50)
    Message = models.CharField(max_length=200)
