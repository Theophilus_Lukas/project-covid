from django.shortcuts import render

from .models import berita
from django.views.generic import ListView

# Create your views here.

class showBeritaViews(ListView):
    model = berita
    template_name = 'berita/berita.html'