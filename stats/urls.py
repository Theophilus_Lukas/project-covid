from django.urls import path
from . import views
from .views import showprovinsiView


app_name = 'stats'

urlpatterns = [
    path('', showprovinsiView.as_view(), name='stathome'),
]