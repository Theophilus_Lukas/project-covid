from django.shortcuts import render, redirect
from .models import Checkin
from .forms import CheckinForm

# Create your views here.

def cuci_tangan(request):
    return render(request, 'cuci_tangan.html')

def di_rumah(request):
    return render(request, 'di_rumah.html')

def di_luar_rumah(request):
    return render(request, 'di_luar_rumah.html')

def add_jurnal(request):
    if request.method == 'POST':
        form = CheckinForm(request.POST)
        if form.is_valid():
            jurnal = form.save()
            jurnal.save()
            return redirect('tips:view_jurnal')
    else:
        form = CheckinForm()

    return render(request, 'edukasi.html',{'form':form})

def view_jurnal(request):
    semua_jurnal = Checkin.objects.order_by('Subject')
    return render(request,'view_jurnal.html',{'semua_jurnal':semua_jurnal})

