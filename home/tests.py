from django.test import TestCase,Client
from django.urls import reverse, resolve
from .views import index
from .models import ShowSubmisi
from bintang.models import Cerita

# Create your tests here.
class HomeTest(TestCase):
    def test_view_index(self):
        found = resolve('/')
        self.assertEqual(found.func, index)
    
    def test_status_code_200_index(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    def test_only_one_showsubmisi_model(self):
        response = self.client.get(reverse('home:index'))
        count= ShowSubmisi.objects.count()
        self.assertEqual(count, 1)

    def test_count_submitted_cerita_model(self):
        # membuat objek show_submisi pada response 1
        response_1 = self.client.get('/')
        show_submisi = ShowSubmisi.objects.all()[0]
        c1 = Cerita(
            nama_pencerita='Kucing',
            email_pencerita='kucing@ui.ac.id',
            judul_pencerita='Hi yall',
            cerita_pencerita='capek bgt bos',
            submisi=show_submisi
        )
        c1.save()
        c2 = Cerita(
            nama_pencerita='Anjing',
            email_pencerita='anjing@ui.ac.id',
            judul_pencerita='Hola yall',
            cerita_pencerita='anjay bos',
            submisi=show_submisi
        )
        c2.save()
        count = show_submisi.cerita_set.count()

        # refresh page
        response_2 = self.client.get('/')
        self.assertContains(response_2, 'Submisi #', count)
        self.assertContains(response_2, '<div class="swiper-submisi-wrapper"')

    def test_nav_bar(self):
        response = self.client.get('/')
        isi = response.content.decode('utf8')
        self.assertIn('<nav>', isi)
        self.assertIn('</nav>', isi)