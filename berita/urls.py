from django.urls import path
from . import views
from .views import showBeritaViews


app_name = 'berita'

urlpatterns = [
    path('', showBeritaViews.as_view(), name='beritahome'),
]