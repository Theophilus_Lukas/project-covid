from django.shortcuts import render
from .models import ShowSubmisi
# Create your views here.

def index(request):
    if ShowSubmisi.objects.all().count() == 0:
        submisi = ShowSubmisi.objects.create(id=1)
    else:
        count = ShowSubmisi.objects.count()
        for i in range(1,count):
            ShowSubmisi.objects.all()[i].delete()
        submisi = ShowSubmisi.objects.all()[0]
        ShowSubmisi.objects.filter(id=submisi.id).update(id=1)
    return render(request, 'home/index.html', {'submisi':submisi})